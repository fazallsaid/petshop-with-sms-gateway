<?php
include "config.php";
$query = mysqli_query($connection, "SELECT * FROM produk");
?>

<div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                                        </i>
                                    </div>
                                    <div>Daftar Produk/jasa
                                        <div class="page-title-subheading">View of tabel Produk dan Jasa
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>           
						<div class="row">
                            <div class="col-lg-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title"></h5>
                                        <table class="mb-0 table table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Kode Produk</th>
                                                <th>Nama Produk</th>
                                                <th>Harga</th>
                                                <th>Aksi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php if (mysqli_num_rows($query) > 0) :
                                             $no = 1;
                                            while ($data = mysqli_fetch_array($query)) :
                                             ?>
                                            <tr>
                                                <th scope="row"><?php echo $no; ?></th>
                                                <td><?php echo $data['code_produk']; ?></td>
                                                <td><?php echo $data['nama_produk']; ?></td>
                                                <td><?php echo rupiah($data['harga_produk']); ?></td>
                                                <td>
                                                <a onclick="this.href='javascript:void(0)';this.disabled=1">Edit</a> |
                                                <a onclick="this.href='javascript:void(0)';this.disabled=1">Hapus</a>
                                                </td>
                                            </tr>
                                            <?php
											$no++;
                                            endwhile;											
                                        endif;?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>