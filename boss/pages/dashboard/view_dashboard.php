<?php include '../kon.php' ?>
<div class="app-main__inner">
    <div class="row">
        <div class="col-md-6 col-xl-4">
            <div class="card mb-3 widget-content bg-midnight-bloom">
                <div class="widget-content-wrapper text-white">
                <?php
                    $qwery = mysqli_query($kon, "SELECT * FROM konsumen");
                    $data1 = mysqli_num_rows($qwery);

                    if ($data1 < 10){
                        $notif1 = "Masih sedikit pelanggan";
                    }else{
                        $notif1 = "Kerja bagus!";
                    }
                    ?>
                    <div class="widget-content-left">
                        <div class="widget-heading">Total Pelanggan</div>
                        <div class="widget-subheading"><?php echo $notif1; ?></div>
                    </div>
                    
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span><?php echo $data1; ?></span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-4">
            <div class="card mb-3 widget-content bg-arielle-smile">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading">Produk</div>
                        <div class="widget-subheading">Total Pembelian Produk</div>
                    </div>
                    <?php
                    $qwery1 = mysqli_query($kon, "SELECT * FROM penjualan");
                    $data2 = mysqli_num_rows($qwery1);
                    ?>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span><?php echo $data2; ?></span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-4">
            <div class="card mb-3 widget-content bg-grow-early">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading">Jasa Grooming</div>
                        <div class="widget-subheading">Jumlah penggunaan jasa</div>
                    </div>
                    <?php
                    $qwery2 = mysqli_query($kon, "SELECT * FROM transaksi WHERE jenis_transaksi='jasa'");
                    $data3 = mysqli_num_rows($qwery2);
                    ?>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span><?php echo $data3;?></span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-xl-none d-lg-block col-md-6 col-xl-4">
            <div class="card mb-3 widget-content bg-premium-dark">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading">Products Sold</div>
                        <div class="widget-subheading">Revenue streams</div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-warning"><span>$14M</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>