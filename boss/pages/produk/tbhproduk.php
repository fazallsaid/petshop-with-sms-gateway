<div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-graph text-success">
                                        </i>
                                    </div>
                                    <div>Tambah Produk/Jasa
                                        <div class="page-title-subheading">Tambahkan Produk/Jasa Anda
                                        </div>
                                    </div>
                                </div>
							</div>
                        </div>           
                        <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
                            <li class="nav-item">
                                <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-content-0">
                                    <span>Produk</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-1">
                                    <span>Jasa</span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                                <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title">Masukkan detail produk Anda</h5>
                                        <form class="" action="?page=proses" method="POST" enctype="multipart/form-data">
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="position-relative form-group">
                                                    <label for="exampleEmail11" class="">Kode Produk</label>
                                                    <input name="code_produk" placeholder="Masukkan kode produk Anda (e.g FOOD0001,ACCSRS0001)" type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="position-relative form-group">
                                                    <label for="exampleEmail11" class="">Nama Produk</label>
                                                    <input name="nama_produk" placeholder="Masukkan nama produk Anda" type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="position-relative form-group">
                                                    <label for="exampleEmail11" class="">Harga Produk</label>
                                                    <input name="harga_produk" placeholder="Masukkan harga produk Anda" type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="position-relative form-group">
                                                    <label for="exampleEmail11" class="">Foto Produk</label>
                                                    <input name="foto" type="file" class="form-control">
                                                    </div>
                                                </div>
                                                <button class="mt-2 btn btn-success">Tambah Produk</button>
                                            </div>
                                            
                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane tabs-animation fade" id="tab-content-1" role="tabpanel">
                            <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title">Masukkan detail jasa Anda</h5>
                                        <form class="" action="?page=proses" method="POST" enctype="multipart/form-data">
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="position-relative form-group">
                                                    <label for="exampleEmail11" class="">Kode Jasa</label>
                                                    <input name="code_produk" placeholder="Masukkan kode jasa Anda (e.g JS0001)" type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="position-relative form-group">
                                                    <label for="exampleEmail11" class="">Nama Jasa</label>
                                                    <input name="nama_produk" placeholder="Masukkan nama jasa Anda" type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="position-relative form-group">
                                                    <label for="exampleEmail11" class="">Harga Jasa</label>
                                                    <input name="harga_produk" placeholder="Masukkan harga jasa Anda" type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="position-relative form-group">
                                                    <label for="exampleEmail11" class="">Foto Ilustrasi</label>
                                                    <input name="foto" type="file" class="form-control">
                                                    </div>
                                                </div>
                                                <button class="mt-2 btn btn-success">Tambah Jasa</button>
                                            </div>
                                            
                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 