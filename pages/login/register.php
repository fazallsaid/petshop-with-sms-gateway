<div class="col-2">
            <div class="box alt">
              <div class="border-top">
                <div class="border-right">
                  <div class="border-bot">
                    <div class="border-left">
                      <div class="left-top-corner">
                        <div class="right-top-corner">
                          <div class="right-bot-corner">
                            <div class="left-bot-corner">
                              <div class="inner">
                                <h3 class="aligncenter">Daftar untuk menikmati semua layanan dari Rembo Petshop</h3>
                                <form action="pages/login/proses_tambah.php" method="POST">
                                <div class="row">
                                  <div class="col-md-6">
                                    <label class="label">Nama Lengkap</label>
                                    <input type="text" class="form_login" name="nama_konsumen" placeholder="Masukkan nama Anda" />
                                  </div>
                                  <br/>
                                  <div class="col-md-6">
                                    <label class="label">alamat</label>
                                    <textarea rows="15" class="form_login" name="alamat"></textarea>
                                  </div>
                                  <div class="col-md-6">
                                    <label class="label">Jenis Kelamin</label>
                                    <select class="form_login" name="jk">
                                    <option value="Pria">Pria</option>
                                    <option value="Wanita">Wanita</option>
                                    </select>
                                  </div>
                                  <div class="col-md-6">
                                    <label class="label">Tanggal Lahir</label>
                                    <input type="date" class="form_login" name="tgl_lahir" />
                                  </div>
                                  <div class="col-md-6">
                                    <label class="label">Nomor KTP</label>
                                    <input type="text" class="form_login" name="no_ktp" placeholder="masukkan nomor ktp Anda" />
                                  </div>
                                  <div class="col-md-6">
                                    <label class="label">Nomor Telepon</label>
                                    <input type="text" class="form_login" name="no_telepon" placeholder="Masukkan nomor telepon Anda (tanpa 0 didepan)" />
                                  </div>
                                  <div class="col-md-6">
                                    <label class="label">Password</label>
                                    <input type="text" class="form_login" name="password" placeholder="Masukkan password" />
                                  </div>
                                  <div class="col-md-6">
                                    <button type="submit" class="tombol_login">Daftar</button>
                                  </div>
                                </div>
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="box1">
              <div class="border-top">
                <div class="border-right">
                  <div class="border-bot">
                    <div class="border-left">
                      <div class="left-top-corner">
                        <div class="right-top-corner">
                          <div class="right-bot-corner">
                            <div class="left-bot-corner">
                              <div class="inner">
                                <h4 class="aligncenter">Recent Articles</h4>
                                <ul class="list">
                                  <li><a href="#">About Pets</a><br />
                                    Learn more about our pets, their habits and behavior. Here you will get the general information about our little friends.</li>
                                  <li><a href="#">About Pets Template</a><br />
                                    Free 1024/768 Optimized Website Template from Templates.com. We really hope that you will like this template and use it for your website. </li>
                                  <li><a href="#">Pet Care</a><br />
                                    Learn how to take care of your pets from this sample article.</li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>