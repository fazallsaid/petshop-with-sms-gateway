<?php
if (isset($_GET['page'])) {
    $page = $_GET['page'];

    switch ($page) {
        case 'dashboard':
            include "pages/dashboard/view_dashboard.php";
            break;
        case 'addproduk':
            include "pages/produk/tbhproduk.php";
            break;
        case 'produk':
                include "pages/produk/produk.php";
                break;
        case 'proses':
            include "pages/produk/proses_tambah.php";
            break;
        default:
            echo "<center><h3>Maaf. Halaman tidak di temukan !</h3></center>";
            break;
    }
} else {
    include "./pages/dashboard/view_dashboard.php";
}
