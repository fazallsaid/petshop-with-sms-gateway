<?php
session_start();

include 'kon.php';
include 'rupiah.php';

if (!isset($_SESSION['no_telepon'])){
  $notif="
  <p>
                            Anda belum masuk. Jika ingin membeli produk / memakai jasa kami, silahkan menjadi pelanggan terlebih dahulu.<br/>
                            <span><a href='?page=login'><img src='images/buttonmasuk.png' width='150' height='50' alt='' /></a></span>
                            <span><a href='?page=register'><img src='images/buttondaftar.png' width='150' height='50' alt='' /></a></span>
                            </p>
  ";
  $menu = "
  <ul>
  <li><a href='./'>Beranda</a></li>
  <li><a href='?page=about'>Tentang Kami</a></li>
  <li><a href='?page=product'>Produk</a></li>
  <li><a href='?page=services'>Jasa</a></li>
</ul> 
  ";
  $keranjangg = "";
}else{
  $notif="<p>Selamat Datang, <b>" . $_SESSION['nama_konsumen'] ."!</b></p>";
  $menu = "
  <ul>
  <li><a href='./'>Beranda</a></li>
  <li><a href='?page=about'>Tentang Kami</a></li>
  <li><a href='?page=product'>Produk</a></li>
  <li><a href='?page=services'>Jasa</a></li>
  <li><a href='?page=cart'>Keranjang Saya</a></li>
  <li><a href='logout.php'>Keluar</a></li>
</ul> 
  ";
  $keranjangg = "<span><a href='#'><img src='images/buttonkeranjang.png' width='150' height='50' alt='' /></a></span>";
}
?>
<!DOCTYPE html>
<html lang="id">
<head>
<title>Pets</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="style.css" rel="stylesheet" type="text/css" />
<link href="https://raw.githubusercontent.com/daneden/animate.css/master/animate.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="js/cufon-yui.js" type="text/javascript"></script>
<script src="js/cufon-replace.js" type="text/javascript"></script>
<!--[if lt IE 7]>
<script type="text/javascript" src="js/ie_png.js"></script>
<script type="text/javascript">ie_png.fix('.png');</script>
<![endif]-->
</head>
<body id="page1">
<div id="main">
  <!-- HEADER -->
  <div id="header">
    <div class="container">
      <div class="row-1">
        <div class="fleft">
          
        </div>
        <div class="fright">
          <?php echo $menu; ?>
        </div>
      </div>
      <div class="row-2">
        <div class="fleft"><a href="./"><img src="images/rembopetshop.png" width="170" height="140" alt="" /></a></div>
        <div class="fright"><h3><font color="white">Sayangi hewan<br/>peliharaan Anda!</font></h3><span><a href="#"><img src="images/button.png" width="150" height="50" alt="" /></a></span></div>
      </div>
      
    </div>
  </div>
  <!-- CONTENT -->
  <div id="content">
    <div class="inner_copy"></div>
    <div class="container">
      <div class="indent">
        <div class="wrapper">
          <div class="col-1">
            <h3>Pembelian Terakhir</h3>
            <ul>
            <?php
            if (!isset($_SESSION['no_telepon'])){
                  $quer = mysqli_query($kon, "SELECT konsumen.*, transaksi.*, penjualan.*, produk.* FROM transaksi 
                JOIN pembelian ON pembelian.id_pembelian=transaksi.id_pembelian 
                JOIN konsumen ON konsumen.id_konsumen=pembelian.id_konsumen 
                JOIN penjualan ON penjualan.id_penjualan=pembelian.id_penjualan
                JOIN produk ON produk.id_produk=penjualan.id_produk
                ORDER BY transaksi.tgl_transaksi DESC LIMIT 5");
                while ($list = mysqli_fetch_assoc($quer)):
                    $hitung = mysqli_num_rows($quer);
                    if ($hitung > 0):
                ?>
                  <li><?php echo $list['tgl_transaksi']; ?><br />
                    <?php echo $list['nama_konsumen']; ?> - <?php echo $list['nama_produk']; ?></li>
                </ul>
                <?php 
                else:
                    echo "Belum ada pembelian";
                endif;
            endwhile;
            }else{
              $quer = mysqli_query($kon, "SELECT konsumen.*, transaksi.*, penjualan.*, produk.* FROM transaksi 
            JOIN pembelian ON pembelian.id_pembelian=transaksi.id_pembelian 
            JOIN konsumen ON konsumen.id_konsumen=pembelian.id_konsumen 
            JOIN penjualan ON penjualan.id_penjualan=pembelian.id_penjualan
            JOIN produk ON produk.id_produk=penjualan.id_produk WHERE konsumen.no_telepon='$_SESSION[no_telepon]'
            ORDER BY transaksi.tgl_transaksi DESC LIMIT 5");
            while ($list = mysqli_fetch_assoc($quer)):
                $hitung = mysqli_num_rows($quer);
                if ($hitung > 0):
            ?>
              <li><?php echo $list['tgl_transaksi']; ?><br />
                <?php echo $list['nama_konsumen']; ?> - <?php echo $list['nama_produk']; ?></li>
            </ul>
            <?php 
            else:
                echo "Belum ada pembelian";
            endif;
        endwhile;
            }
              ?>
            <div class="banner"><a href="#"><img src="images/banner.png" alt="" class="png" /></a></div>
          </div>
          <?php include 'select.php' ?>
        </div>
      </div>
    </div>
  </div>
  <!-- FOOTER -->
  <?php include 'kakii.php' ?>
</body>
</html>
