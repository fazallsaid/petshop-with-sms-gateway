<div class="app-sidebar__inner">
    <ul class="vertical-nav-menu">
        <li class="app-sidebar__heading">Dasbor</li>
        <li>
            <a href="?page=dashboard">
                <i class="metismenu-icon pe-7s-rocket"></i>
                Dasbor
            </a>
        </li>
        <li class="app-sidebar__heading">Produk</li>
        <li>
            <a href="?page=produk">
                <i class="metismenu-icon pe-7s-look"></i>
                View Produk
            </a>
        </li>
        <li>
            <a href="?page=addproduk">
                <i class="metismenu-icon pe-7s-plus"></i>
                Add Produk
            </a>
        </li>
    </ul>
</div>